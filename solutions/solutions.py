# 1. Function to get all keys from a dictionary
def get_all_keys(dictionary):
    return list(dictionary.keys())

# 2. Function to merge two dictionaries
def merge_dictionaries(dict1, dict2):
    merged_dict = dict1.copy()
    merged_dict.update(dict2)
    return merged_dict

# 3. Function to check if a key exists in a dictionary
def check_key(dictionary, key):
    return key in dictionary

# 4. Function to filter dictionaries based on key-value pair
def filter_dictionaries(dictionaries, key, value):
    result = []
    for d in dictionaries:
        if d.get(key) == value:
            result.append(d)
    return result

# 5. Function to count word frequency in a sentence
def count_word_frequency(sentence):
    word_list = sentence.split()
    word_freq = {}
    for word in word_list:
        word_freq[word] = word_freq.get(word, 0) + 1
    return word_freq

# 6. Function to sort a dictionary by common keys
def find_common_keys(dict1, dict2):
    common_keys = []
    for key in dict1.keys():
        if key in dict2:
            common_keys.append(key)
    return common_keys

# 7. Function to calculate the sum of dictionary values
def calculate_sum(dictionary):
    return sum(dictionary.values())

# 8. Function to find the key with maximum value in a dictionary
def find_max_key(dictionary):
    max_key = max(dictionary, key=dictionary.get)
    return max_key

# 9. Function to extract unique values from a list of dictionaries
def extract_unique_values(dictionaries):
    unique_values = set()
    for d in dictionaries:
        unique_values.update(d.values())
    return unique_values

# 10. Function to filter dictionary by integer values
def filter_int_values(dictionary):
    filtered_dict = {k: v for k, v in dictionary.items() if isinstance(v, int)}
    return filtered_dict

# 11. Function to check if two dictionaries are equal
def check_dicts_equality(dict1, dict2):
    return dict1 == dict2

# 12. Function to extract a specific field from dictionaries
def extract_field_from_dicts(dictionaries, field):
    field_values = [d.get(field) for d in dictionaries]
    return field_values

# 13. Function to check if all values in a dictionary are unique
def check_unique_values(dictionary):
    return len(set(dictionary.values())) == len(dictionary)

# 14. Function to calculate the average value of numbers in a dictionary
def calculate_average(dictionary):
    values = [v for v in dictionary.values() if isinstance(v, (int, float))]
    if values:
        return sum(values) / len(values)
    return 0

# 15. Function to merge dictionaries with common keys into a list
def merge_dicts_common_keys(dict1, dict2):
    merged_dict = {}
    for key in set(dict1.keys()).intersection(dict2.keys()):
        merged_dict[key] = [dict1[key], dict2[key]]
    return merged_dict

# Problem 16
def count_nested_keys(dictionary):
    count = 0
    for value in dictionary.values():
        if isinstance(value, dict):
            count += count_nested_keys(value)
            count += 1
    return count

# Problem 17
def remove_duplicate_values(dictionary):
    unique_values = set()
    unique_dict = {}
    for key, value in dictionary.items():
        if value not in unique_values:
            unique_values.add(value)
            unique_dict[key] = value
    return unique_dict

# Problem 18
def convert_list_of_dicts_to_dict_of_lists(dictionaries):
    result = {}
    for dictionary in dictionaries:
        for key, value in dictionary.items():
            if key not in result:
                result[key] = []
            result[key].append(value)
    return result

# Problem 19
def find_min_value_key(dictionary):
    min_value = min(dictionary.values())
    for key, value in dictionary.items():
        if value == min_value:
            return key

# Problem 20
def find_deepest_nested_key(dictionary):
    deepest_key = None
    max_depth = 0

    def find_depth(key, value, depth):
        nonlocal deepest_key, max_depth
        if isinstance(value, dict):
            for nested_key, nested_value in value.items():
                find_depth(nested_key, nested_value, depth + 1)
        elif depth > max_depth:
            deepest_key = key
            max_depth = depth

    for key, value in dictionary.items():
        find_depth(key, value, 1)

    return deepest_key

#########(Dictionary Compehension)#######

# Problem 4
def filter_dictionaries(dictionaries, key, value):
    return [d for d in dictionaries if d.get(key) == value]

# Problem 17
def remove_duplicate_values(dictionary):
    unique_values = set(dictionary.values())
    return {key: value for key, value in dictionary.items() if value in unique_values}

# Problem 18
def convert_list_of_dicts_to_dict_of_lists(dictionaries):
    return {key: [dictionary[key] for dictionary in dictionaries if key in dictionary] for key in set().union(*dictionaries)}

# Problem 19
def find_min_value_key(dictionary):
    min_value = min(dictionary.values())
    return [key for key, value in dictionary.items() if value == min_value][0]
